var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res){
  //res.send('Hola Mundo');
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function(req, res){
  res.send('Hemos recibido su petición cambiada');
})


// get --> JSON de 4 clientes
// post --> Mensaje "Su cliente ha sido dado de alta"
// put --> Mensaje "Su cliente ha sido modificado"
// delete --> Mensaje "Su cliente ha sido dado de baja"

app.get('/clientes/:idcliente', function(req, res){
  //res.send('Aquí tiene al cliente número ' + req.params.idcliente);
  res.sendFile(path.join(__dirname, 'clientes_param' + req.params.idcliente + '.json'));
})


app.get('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'get.json'));
})

app.post('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'post.json'));
})

app.put('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'put.json'));
})

app.delete('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'delete.json'));
})
